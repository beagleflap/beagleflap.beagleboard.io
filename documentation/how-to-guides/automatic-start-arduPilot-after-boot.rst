.. _automatic-start-ardupilot-after-boot:

Automatic start ArduPilot after boot
##############
If ArduPilot should start automatically at boot time follow the instructions below:

1. Connect to your BeagleBone via ssh with ``ssh debian@beaglebone``
2. Edit ``/etc/rc.local`` with ``sudo vi /etc/rc.local``
3. Modify file to (use your ArduPilot file and parameter):
4. Save the file ``esc + :x!``
5. Reboot 

