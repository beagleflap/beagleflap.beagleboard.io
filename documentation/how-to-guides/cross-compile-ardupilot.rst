.. _cross-compile-ardupilot:

Cross compiling ArduPilot from source
=====================================================================================
Compiling ArduPilot on the BeagleBone Black takes a lot of time (several hours) therefore not recommended.

Get the source code:

1. ``cd ~``
2. ``git clone https://github.com/ardupilot/ardupilot.git``
3. ``cd ardupilot``
4. ``./Tools/environment_install/install-prereqs-ubuntu.sh``
5. ArduPlane ``git checkout Plane-4.4`` 
6. ``git submodule update --init --recursive``
7. ``./waf configure --board=bbbmini``
8. ``./waf plane``

	binary is located in ``build/bbbmini/bin/``.
9. ``scp build/bbbmini/bin/* debian@beaglebone:/home/debian/``

 
