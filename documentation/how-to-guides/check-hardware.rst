.. _check-hardware:

Check hardware
##############
If your board is ready you can check that everything is working as expected.

Check IMU
=========
``sudo /home/debian/INS_generic``

Check baro
==========
``sudo /home/debian/BARO_generic``

Check GPS
=========
``sudo /home/debian/GPS_AUTO_test -B /dev/ttyO5``

Check rcinput
=============

``sudo /home/debian/RCInput``
