.. _automatic-start-ardupilot-after-boot:

Automatic start ArduPilot after boot
##############
If ArduPilot should start automatically at boot time follow the instructions below:

1. Connect to your BeagleBone via ssh with ``ssh debian@beaglebone``
2. Edit ``/etc/rc.local`` with ``sudo vi /etc/rc.local``
3. Modify file to (use your ArduPilot file and parameter)::

		
	#!/bin/sh -e
	#
	# rc.local
	#
	# This script is executed at the end of each multiuser runlevel.
	# Make sure that the script will "exit 0" on success or any other
	# value on error.
	#
	# In order to enable or disable this script just change the execution
	# bits.
	#
	# By default this script does nothing.

	/bin/sleep 10
	/home/debian/arducopter 

	exit 0

4. Save the file ``esc + :x!``
5. Reboot 


