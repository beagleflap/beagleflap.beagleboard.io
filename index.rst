.. beagleflap.beagleboard.io documentation master file, created by
   sphinx-quickstart on Fri Jan 19 02:18:33 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:html_theme.sidebar_secondary.remove: true
:sd_hide_title: true

BeagleFlap | BeagleBoard.org
#######################

Design, Fabrication and Control of the Flapping Wing Robot ‘Beagle-Flap’
##############

Towards building a flapping wing aerial robot with Linux-based AutoPilot, Using Beagle as a flight controller. 

.. image:: _images/beagleflap-cover.jpg


Beagle-Flap project aims to use a Linux-based autopilot for bio-inspired flapping wing robot’s using the Beagle 
and a custom cape as hardware blueprints and `DroneForce <https://dfautopilot.com>`_, a framework to ship custom 
controller (offboard) with Ardupilot as the software one to allow experimentation and development of control and 
navigation algorithms.

You can reproduce Beagle-Flap robot from scratch the whole design is built to be manufactured cheaply and quickly 
with standard 3D printing and a 2D plate cutter and buying a few standard mechanical parts together with some 
off-the-shelf electronics and a BeagleBone Black, BBBmini. With a few steps you compile ArduPilot, and flash it to the Beagle and fly, crash and fly again.


.. grid:: 1 1 1 3
   :margin: 4 4 0 0
   :gutter: 2

   .. grid-item-card::
      :link: https://git.beagleboard.org/krvprashanth/beagle-flap

      :fab:`gitlab;fa-fade pst-color-dark` BeagleFlap Design Files
      ^^^^^^^^^^

      CAD models! Here you can find STL files for 3D printing and dxf files for lazer cutting or modify parts so you can customize your BeagleFlap! 

   .. grid-item-card::
      :link: https://github.com/mirkix/BBBMINI

      :fas:`microchip;fa-fade pst-color-dark`  BeagleBone ArduPilot DIY Cape 
      ^^^^^^^^^^

      BBBMINI is a simple ArduPilot DIY Cape for the BeagleBone Black which is designed by Mirko Denecke.


.. admonition:: Did you know?

   BeagleBoard.org mentored the original port of ArduPilot to Linux in 2014 as part of Google Summer of Code project.
       
      - BeagleBone Black + Erle Cape or Pixhawk Fire Cape

   Mirko Denecke and ArduPilot community continued work

      - 2015: BBBmini Cape for BeagleBone Black and SeeedStudio BeagleBone Green
      - 2017: Support for BeagleBone Blue
      - 2018: PocketPilot PocketCape for PocketBeagle
      - 2022: BBBMINI V2 Juvinski


.. admonition:: Using Beagle as a Flight Controller
   :class: hint

   It was really intresting to see the `BBBmini port <https://github.com/ArduPilot/ardupilot/pull/1764>`_ which is on beagle bone black with a much much cheaper simpler sensor 
   board on the top more of a sort of Do-it-yourself cape put together and the patch was really nicely structured the entire 
   patch to add support for ardupilot within 43 lines tiny. It looking like it would be easy to throw together another one that’s 
   easier and cheaper for people to put together on top of the Beagle.

   Learn more at https://openbeagle.org/ardupilot-on-beagle
      Repositories of multiple autopilot boards and prototype ports. There has been a good amount of work on flying ports developed to run ArduPilot on Beagle.


.. admonition:: Some inspirational past work


   .. grid:: 1 1 1 3
      :margin: 4 4 0 0
      :gutter: 4

      .. grid-item::

         .. youtube:: -giV6Xr8RtY
            :width: 100%

      .. grid-item::

         .. youtube:: SqyfN3FStvs
            :width: 100%

      .. grid-item::

         .. youtube:: BBnUvO6x0oY   
            :width: 100%
            
            
            
 .. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Documentation

    /documentation/index

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Project Progress

   /project-progress/index



