.. _robot-design-experimentation:

Robot Design & Experimentation
##############################


First Phase of Development
==========================
- First prototype of Beagle-Flap, December 2023
- Using BeagleBone Black + BBBmini as a Flight Controller
- Presented at Maker Faire Hyderabad & ROSCon India 2023
	
https://www.beagleboard.org/blog/2023-12-13-maker-faire-hyderabad-roscon-bangalore-december-2023-events
        
 .. grid:: 1 1 1 3
      :margin: 5 5 0 0
      :gutter: 4

      .. grid-item::

         .. youtube:: KV5Ve06NTys
            :width: 100%
            :height: 100%
            
      .. grid-item::

         .. youtube:: uAbiRCLeok4
            :width: 100%
            :height: 100%

      .. grid-item::

         .. youtube:: 10NQyajT5Ao   
            :width: 100%
            :height: 100%
   
- Flight testing of Beagle-Flap





