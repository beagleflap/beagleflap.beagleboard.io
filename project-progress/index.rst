.. _project-progress:


Project Progress
********


These are the logs of project, where I provide report on the work done.

.. toctree::
   :maxdepth: 2

   /project-progress/robot-design-experimentation/index
   /project-progress/pictures-video/index
   

